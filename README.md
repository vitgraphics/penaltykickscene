# Penalty Kick

The project was created as a part of Graphics and Multimedia Course's corriculum.
Project is based on Unity and simulates Penalty Kick of Football (or Soccer).

### Version

2.5.3

### Deployment

The Project is deployed on various platforms.
Deployments can be found in /Assets/proj_things/

| Directory | Deployment |
| --- | --- |
| And | [Android Deployment][and_link] |
| Lin | [Linux Deployment][lin_link] |
| Win | [Windows Deployment][win_link] |
| Web | [Web Player Deployment][web_link] |

> **Note :** Android deployment works but the game crashes after abruptly. (Dunno why),

> Linux deployment (doesn't work), 

> Windows Deployment (best one, do try, looks awesome)

> Web (doesn't work, and depreciated by Unity)

### Assets

We have used an [Assets package][unity_ass] from Unity Store as  a base to develop this Project. **Big Thanks** to them for creating such a great package.

### Contributers

[Amol Gautam] [amol_git] and 
[Bharat Saraswat] [bhansa_git]

### License

MIT

  [bhansa_git]: <https://github.com/bhansa>
  [amol_git]: <https://github.com/amolgautam25>
  [and_link]: <https://github.com/MrL1605/Penalty-Kick/tree/master/Assets/proj_things/And>
  [lin_link]: <https://github.com/MrL1605/Penalty-Kick/tree/master/Assets/proj_things/Lin>
  [win_link]: <https://github.com/MrL1605/Penalty-Kick/tree/master/Assets/proj_things/Win>
  [web_link]: <https://github.com/MrL1605/Penalty-Kick/tree/master/Assets/proj_things/Web>
  [unity_ass]: <https://www.assetstore.unity3d.com/en/#!/content/3346>
  

[//]: # (Cooments here)


